#include <iostream>
#include <math.h>
using namespace std;

class MyVector
{   
    private:
        double x;
        double y;
        double z;
    public:
        MyVector() : x(3.0), y(4.0), z(5.0)
        {}
        void ShowMyVector()
        {
            cout << "My Vector:\nx = " << x << ", y = " << y << ", z = " << z << endl;
        }
        void VectorLength()
        {
            cout << "My Vector Length:\n" << sqrt(x * x + y * y + z * z) << endl;
        }
};

int main()
{
    MyVector V;
    V.ShowMyVector();
    V.VectorLength();
}